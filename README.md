Usage:
    -ruby main.rb <filename.txt>
    -cat <filename.txt> | ruby main.rb
    -ruby main.rb < <filename.txt>

Test:
    -ruby main.rb samples/spain_league_input.txt
    -cat samples/spain_league_input.txt | ruby main.rb
    -ruby main.rb < samples/spain_league_input.txt

    To see the expected output check samples/spain_league_output.txt

    or

    -ruby main.rb samples/italian_league_input.txt
    -cat samples/italian_league_input.txt | ruby main.rb
    -ruby main.rb < samples/italian_league_input.txt

    To see the expected output check spain_league_output.txt

