class Game

  attr_writer :score_challenging, :score_challenged
  attr_accessor :challenged, :challenging
  attr_reader :tie, :winner, :looser

  def initialize(challenging, challenged, score_challenging, score_challenged)
    @challenging = challenging
    @challenged = challenged
    @score_challenging = score_challenging.to_i
    @score_challenged = score_challenged.to_i
    @tie = false
    set_results
  end

  def set_results
    if @score_challenging == @score_challenged
      @tie = true
    elsif @score_challenging >= @score_challenged
      @winner = @challenging
      @looser = @challenged
    else
      @winner = @challenged
      @looser = @challenging
    end
  end

end