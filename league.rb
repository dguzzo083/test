class League
  attr_accessor :teams, :games, :positions_table

  def initialize
    @positions_table = []
    @games = []
    @teams = {}
  end

  def set_positions
    @games.each do |game|
      if game.tie
        @teams[game.challenging.name] += 1
        @teams[game.challenged.name] += 1
      else
        @teams[game.winner.name] += 3
      end
    end
  end

  def get_positions_table
    get_positions
    @positions_table.each do |position|
      position.teams.each do |team|
        puts "#{position.rank}. #{team}, #{position.points} pts"
      end
    end
  end

  def set_game(team1, team2)
    challenging = Team.new(team1.split(' ')[0...-1].join(' '))
    challenged  = Team.new(team2.split(' ')[0...-1].join(' '))
    self.teams[challenging.name] = 0
    self.teams[challenged.name] = 0
    score_challenging = team1.split().last
    score_challenged = team2.split().last
    game = Game.new(challenging, challenged, score_challenging, score_challenged)
    self.games << game
  end

  def get_positions
    var = 1
    @teams.length.times do
      new_hash = @teams.select {|key,value| value == @teams.values.max}
      max_value = @teams.values.max
      teams = []
      new_hash.each do |key, value|
        teams << key
        @teams.delete(key)
      end

      unless teams.empty?
        position = Position.new(var, teams.sort, max_value)
        @positions_table << position
      end

      var += teams.length
    end
  end

end