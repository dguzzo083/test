class Position
  attr_accessor :rank, :teams, :points

  def initialize(rank, teams, points)
    @rank = rank
    @teams = teams
    @points = points
  end

end