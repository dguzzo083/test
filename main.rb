require_relative 'position'
require_relative 'league'
require_relative 'game'
require_relative 'team'


input = ARGF.read

league = League.new

input.each_line do |line|
  league.set_game(line.split(',')[0], line.split(',')[1])
end

league.set_positions
league.get_positions_table